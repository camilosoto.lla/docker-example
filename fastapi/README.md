# Docker FastAPI example

This is a Python application for docker testing.

## Docker Image
Creating docker image
```sh
docker build --tag apiexample .
```
Check if the docker image was created.
```sh
docker images
```
![docker imange list](../assets/img/fastapiimagelist.png)

## Running docker container
```sh
docker run --publish=8080:80 apiexample
```
check the result opening [http://localhost:8080/docs](http://localhost:8080/docs) 




