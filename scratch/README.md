# Docker from Scratch

## Compile the app
```sh
gcc --static example/hello.cpp -o helloworld.exe
./helloworld.exe
```
This is a simple C++ program which prints:  
**Hello LLA! This message is coming from a container**

## Docker Image
Creating docker image
```sh
docker build --tag hello .
```
Check if the docker image was created.
```sh
docker images
```
![docker imange list](../assets/img/imagesList.png)

## Running docker container
```sh
docker run hello
```
check the result
